from random import randint

import pygame

from config import sc
from light import light_create

bullet_image = pygame.image.load("ASSETS/sprite/bullet/1.png").convert_alpha()


class Bullet(pygame.sprite.Sprite):
    def __init__(self, group):
        super().__init__()
        self.image = bullet_image
        self.size = randint(20, 30)
        self.image = pygame.transform.scale(self.image, (self.size, self.size))
        type_pos_x = randint(0, 1)
        if type_pos_x == 0:
            pos_x = -25
            pos_y = randint(10, 690)
            self.speed_x = randint(1, 6)
            self.speed_y = randint(-1, 1)
        else:
            pos_y = randint(5, 690)
            pos_x = 1250
            self.speed_x = randint(-6, -1)
            self.speed_y = randint(-1, 1)
        self.rect = self.image.get_rect(center=(pos_x, pos_y))

        self.add(group)

    def update(self):
        sc.blit(light_create(self.size-1, (50, 0, 0)), (self.rect.centerx - self.size+1, self.rect.centery - self.size+1),
                special_flags=pygame.BLEND_RGB_ADD)

        if self.rect.y <= -55 or self.rect.y >= 790:
            self.kill()

        self.rect.move_ip(self.speed_x, self.speed_y)
