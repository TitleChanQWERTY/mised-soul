import pygame
from scene_start import select_language_set

icon = pygame.image.load("ASSETS/icon/1.png").convert_alpha()

pygame.mouse.set_visible(False)
pygame.display.set_caption("[Mised Soul]")
pygame.display.set_icon(icon)

if __name__ == "__main__":
    select_language_set()

pygame.quit()
