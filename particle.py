from random import randint

from config import sc
from light import light_create

import pygame


class Particle(pygame.sprite.Sprite):
    def __init__(self, x, y, speed_x, speed_y, image, size, group, time_die=55000, isDie=True, surf=sc):
        super().__init__()
        self.st_image = image
        self.image = image
        self.size = size
        self.image = pygame.transform.scale(image, size)
        self.rect = self.image.get_rect(center=(x, y))

        self.add(group)

        self.speed_x = speed_x
        self.speed_y = speed_y

        self.x = x
        self.y = y

        self.angle: int = 0
        self.angle_speed = randint(-5, 5)

        self.max_time_die = time_die
        self.time_die = 0

        if self.angle_speed == 0:
            self.angle_speed = 1

        self.alpha = randint(100, 200)

        self.isDie = isDie

        self.surf = surf

    def update(self):
        if self.isDie:
            if self.time_die >= self.max_time_die:
                self.alpha -= 15
                if self.alpha <= 0:
                    self.kill()
        if self.rect.y <= -15 or self.rect.y >= 720:
            self.kill()
        self.time_die += 1
        self.x += self.speed_x
        self.y += self.speed_y

        self.image = self.st_image
        self.image = pygame.transform.scale(self.image, self.size)
        self.image.set_alpha(self.alpha)
        self.angle += self.angle_speed
        self.image = pygame.transform.rotate(self.image, self.angle)
        self.rect = self.image.get_rect(center=(self.x, self.y))

        self.surf.blit(light_create(4, (50, 50, 50)), (self.rect.centerx - 4, self.rect.centery - 4),
                       special_flags=pygame.BLEND_RGB_ADD)
