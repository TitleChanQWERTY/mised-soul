import pygame
import config
import language_system
from scene_menu import scene_m


def start_logo():
    running = True
    FPS: int = 60

    image_title_chan = pygame.image.load("ASSETS/ui/TitleLogoMan!.png").convert_alpha()
    image_title_chan = pygame.transform.scale(image_title_chan, (489, 300))

    TitleChanQWERTY = pygame.font.Font("ASSETS/font/osifont.ttf", 26).render("POWERED BY TitleChanQWERTY", True, (255, 255, 255))

    frame = 0
    alpha = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False

        config.sc.fill((0, 0, 0))
        if frame < 120:
            alpha += 3
            image_title_chan.set_alpha(alpha)
            TitleChanQWERTY.set_alpha(alpha)
        if frame > 121:
            alpha -= 5
            image_title_chan.set_alpha(alpha)
            TitleChanQWERTY.set_alpha(alpha)
        if frame > 255:
            running = False
            scene_m()
        frame += 1
        config.sc.blit(image_title_chan, image_title_chan.get_rect(center=(config.WINDOW_SIZE[0]/2, config.WINDOW_SIZE[1]/2-5)))
        config.sc.blit(TitleChanQWERTY, TitleChanQWERTY.get_rect(center=(config.WINDOW_SIZE[0]/2-5, config.WINDOW_SIZE[1]/2+250)))
        pygame.display.update()
        config.clock.tick(FPS)


def select_screen():
    running = True
    FPS: int = 60

    eng_lang = pygame.font.Font("ASSETS/font/osifont.ttf", 30).render("Fullscreen", True, (255, 255, 255))
    ukr_lang = pygame.font.Font("ASSETS/font/osifont.ttf", 30).render("Windowed", True, (255, 255, 255))

    select = 0

    selectorImage = pygame.image.load("ASSETS/ui/selector.png").convert_alpha()
    selectorImage = pygame.transform.scale(selectorImage, (43 * 5, 19 * 3))

    yPosSelector = config.WINDOW_SIZE[1] / 2 - 150

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_RETURN:
                    if select == 0:
                        config.sc = pygame.display.set_mode(config.WINDOW_SIZE, pygame.SCALED | pygame.FULLSCREEN)
                    running = False
                    start_logo()
                if e.key == pygame.K_DOWN or e.key == pygame.K_s:
                    select += 1
                if e.key == pygame.K_UP or e.key == pygame.K_w:
                    select -= 1
                select %= 2

        config.sc.fill((0, 0, 0))
        if select == 0:
            yPosSelector = config.WINDOW_SIZE[1] / 2 - 50
        if select == 1:
            yPosSelector = config.WINDOW_SIZE[1] / 2 + 50
        config.sc.blit(eng_lang, eng_lang.get_rect(center=(config.WINDOW_SIZE[0] / 2, config.WINDOW_SIZE[1] / 2 - 50)))
        config.sc.blit(ukr_lang, ukr_lang.get_rect(center=(config.WINDOW_SIZE[0] / 2, config.WINDOW_SIZE[1] / 2 + 50)))
        config.sc.blit(selectorImage, selectorImage.get_rect(center=(config.WINDOW_SIZE[0] / 2, yPosSelector)))
        pygame.display.update()
        config.clock.tick(FPS)


def select_language_set():
    running = True
    FPS: int = 60

    eng_lang = pygame.font.Font("ASSETS/font/osifont.ttf", 30).render("English", True, (255, 255, 255))
    ukr_lang = pygame.font.Font("ASSETS/font/osifont.ttf", 30).render("Українська", True, (255, 255, 255))

    select = 0

    selectorImage = pygame.image.load("ASSETS/ui/selector.png").convert_alpha()
    selectorImage = pygame.transform.scale(selectorImage, (43 * 5, 19 * 3))

    yPosSelector = config.WINDOW_SIZE[1] / 2 - 150

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_RETURN:
                    language_system.language(select)
                    running = False
                    select_screen()
                if e.key == pygame.K_DOWN or e.key == pygame.K_s:
                    select += 1
                if e.key == pygame.K_UP or e.key == pygame.K_w:
                    select -= 1
                select %= 2

        config.sc.fill((0, 0, 0))
        if select == 0:
            yPosSelector = config.WINDOW_SIZE[1] / 2 - 50
        if select == 1:
            yPosSelector = config.WINDOW_SIZE[1] / 2 + 50
        config.sc.blit(eng_lang, eng_lang.get_rect(center=(config.WINDOW_SIZE[0] / 2, config.WINDOW_SIZE[1] / 2 - 50)))
        config.sc.blit(ukr_lang, ukr_lang.get_rect(center=(config.WINDOW_SIZE[0] / 2, config.WINDOW_SIZE[1] / 2 + 50)))
        config.sc.blit(selectorImage, selectorImage.get_rect(center=(config.WINDOW_SIZE[0] / 2, yPosSelector)))
        pygame.display.update()
        config.clock.tick(FPS)
