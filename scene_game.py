from random import randint

import pygame

import language_system
import scene_menu
from config import sc, clock, WINDOW_SIZE
from light import light_create
from particle import Particle
from bullet import Bullet
from scene_final import scene

particle_image = (pygame.image.load("ASSETS/sprite/particle/1_1.png").convert_alpha(),
                  pygame.image.load("ASSETS/sprite/particle/2_2.png").convert_alpha(),
                  pygame.image.load("ASSETS/sprite/particle/1.png").convert_alpha(),
                  pygame.image.load("ASSETS/sprite/particle/2.png").convert_alpha(),
                  pygame.image.load("ASSETS/sprite/particle/3.png").convert_alpha(),
                  pygame.image.load("ASSETS/sprite/particle/4.png").convert_alpha())

particle_group = pygame.sprite.Group()
bullet_group = pygame.sprite.Group()

collect_soul = 0

max_collect_bullet = randint(2, 3)

fillGroup = pygame.sprite.Group()

sfx_collect_soul = pygame.mixer.Sound("ASSETS/sfx/TAKE_SOUL.ogg")


class fillScreen(pygame.sprite.Sprite):
    def __init__(self, color, speed_fill=-5, alpha=250, alpha_max=0):
        super().__init__()
        self.image = pygame.Surface((WINDOW_SIZE[0], WINDOW_SIZE[1]))
        self.image.fill(color)
        self.rect = self.image.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2))

        self.add(fillGroup)

        self.alpha = alpha
        self.speed_fill = speed_fill
        self.alpha_max = alpha_max

    def update(self):
        if self.alpha <= self.alpha_max:
            self.kill()
        self.alpha += self.speed_fill
        self.image.set_alpha(self.alpha)


def scene_g():
    global collect_soul
    for particle in particle_group:
        particle.kill()

    running: bool = True
    FPS: int = 60

    circleRect = pygame.Rect(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2, 16, 16)

    soulRect = pygame.Rect(randint(15, WINDOW_SIZE[0] - 50), randint(55, WINDOW_SIZE[1] - 55), 16, 16)

    colorFillScreen = (0, 0, 0)
    colorFillCircle = (255, 255, 255)

    isTakeSoul = False

    time_restart_take = 0

    time_create_bullet = 50

    pygame.mixer.music.set_volume(0.1)

    isPause: bool = False

    txtResume = pygame.font.Font("ASSETS/font/osifont.ttf", 30).render(language_system.txt_lang["RESUME"], True,
                                                                       (255, 255, 255))
    txtMainMenu = pygame.font.Font("ASSETS/font/osifont.ttf", 30).render(language_system.txt_lang["BACK_TO_MAIN_MENU"],
                                                                         True,
                                                                         (255, 255, 255))

    select: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_ESCAPE:
                    if not isPause:
                        isPause = True
                    else:
                        isPause = False
                if isPause:
                    if e.key == pygame.K_RETURN or e.key == pygame.K_e:
                        if select == 0:
                            isPause = False
                        if select == 1:
                            collect_soul = 0
                            running = False
                            scene_menu.scene_m()
                    if e.key == pygame.K_DOWN or e.key == pygame.K_s:
                        select += 1
                    if e.key == pygame.K_UP or e.key == pygame.K_w:
                        select -= 1
                    select %= 2

        sc.fill(colorFillScreen)
        if not isPause:
            particle_group.draw(sc)
            particle_group.update()

            if pygame.mouse.get_pressed()[0] or pygame.mouse.get_pressed()[2]:
                Particle(circleRect.centerx, circleRect.centery, 0, 0,
                         particle_image[5],
                         (5, 5), particle_group, randint(100, 140))

            if not isTakeSoul:
                if collect_soul >= max_collect_bullet:
                    if time_create_bullet <= 0:
                        Bullet(bullet_group)
                        time_create_bullet = 50
                    time_create_bullet -= 1

                txtCollectSoul = pygame.font.Font("ASSETS/font/osifont.ttf", 22).render(
                    language_system.txt_lang["COLLECT_SOUL"] + ": " + str(collect_soul) + "/15", True, (255, 255, 255))
                sc.blit(txtCollectSoul, txtCollectSoul.get_rect(center=(WINDOW_SIZE[0] // 2, 25)))

                xMouse, yMouse = pygame.mouse.get_pos()
                circleRect.centerx = xMouse
                circleRect.centery = yMouse
                pygame.draw.circle(sc, colorFillCircle, circleRect.center, 15)

                createParticle = randint(0, 2)
                if createParticle == 1:
                    size = randint(2, 5)
                    Particle(circleRect.centerx, circleRect.centery, randint(-2, 2), randint(-1, 1),
                             particle_image[randint(2, 3)],
                             (size - 2, size + 9), particle_group, randint(23, 55))
                    Particle(soulRect.centerx, soulRect.centery, randint(-2, 2), randint(-1, 1),
                             particle_image[4],
                             (size + 3, size + 3), particle_group, randint(23, 55))

                color_light_first = randint(20, 55)
                pygame.draw.circle(sc, (150, 150, 150), soulRect.center, 14)
                sc.blit(light_create(21, (color_light_first, color_light_first, color_light_first)),
                        (soulRect.centerx - 21, soulRect.centery - 21),
                        special_flags=pygame.BLEND_RGB_ADD)

                sc.blit(light_create(18, (55, 55, 55)), (circleRect.centerx - 18, circleRect.centery - 18),
                        special_flags=pygame.BLEND_RGB_ADD)
                sc.blit(light_create(21, (52, 52, 52)), (circleRect.centerx - 21, circleRect.centery - 21),
                        special_flags=pygame.BLEND_RGB_ADD)

                if circleRect.colliderect(soulRect):
                    sfx_collect_soul.play()
                    if collect_soul == 0:
                        pygame.mixer.music.load("ASSETS/ost/Take knife, and kill my soul.mp3")
                        pygame.mixer.music.play(-1)
                    collect_soul += 1
                    for i in range(25):
                        size = randint(4, 6)
                        Particle(soulRect.centerx, soulRect.centery, randint(-6, 6), randint(-5, 5),
                                 particle_image[randint(0, 1)], (size - 2, size + 8), particle_group)
                    isTakeSoul = True
                    pygame.mixer.music.set_volume(0.2)
                for bullet in bullet_group:
                    if bullet.rect.collidepoint(circleRect.center):
                        collect_soul = 0
                        fillScreen((255, 0, 0))
                        bullet.kill()
                        running = False
                        scene_g()
                bullet_group.draw(sc)
                bullet_group.update()
            else:
                colorFillScreen = (255, 255, 255)
                if time_restart_take >= 130:
                    for fill in fillGroup:
                        fill.kill()
                    if collect_soul < 15:
                        running = False
                        scene_g()
                    else:
                        for bullet in bullet_group:
                            bullet.kill()
                        collect_soul = 0
                        running = False
                        scene()

                if time_restart_take == 70:
                    fillScreen((0, 0, 0), 5, 15)
                time_restart_take += 1
            fillGroup.draw(sc)
            fillGroup.update()
        else:
            if select == 0:
                selectorImage = pygame.image.load("ASSETS/ui/selector.png").convert_alpha()
                selectorImage = pygame.transform.scale(selectorImage, (43 * 5, 19 * 3))
                yPosSelector: int = WINDOW_SIZE[1] / 2 - 55
            else:
                selectorImage = pygame.image.load("ASSETS/ui/selector.png").convert_alpha()
                selectorImage = pygame.transform.scale(selectorImage, (44 * 5, 19 * 3))
                yPosSelector: int = WINDOW_SIZE[1] / 2 + 45

            colorFillScreen = (0, 0, 0)
            sc.blit(txtResume, txtResume.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 - 50)))
            sc.blit(txtMainMenu, txtMainMenu.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 + 50)))

            sc.blit(selectorImage, selectorImage.get_rect(center=(WINDOW_SIZE[0] / 2, yPosSelector)))

        fpsTXT = pygame.font.Font("ASSETS/font/osifont.ttf", 15).render("FPS: " + str(int(clock.get_fps())), True,
                                                                        (150, 150, 150))
        sc.blit(fpsTXT, fpsTXT.get_rect(center=(30, 15)))

        pygame.display.update()
        clock.tick(FPS)
