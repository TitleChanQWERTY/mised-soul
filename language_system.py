import json
import os


def check_lang():
    if not os.path.exists("Language"):
        os.mkdir("Language")
    if not os.listdir("Language"):
        data = {
            "CREATED_BY": "CREATED BY TitleChanQWERTY",

            "RESUME": "RESUME",
            "COLLECT_SOUL": "COLLECT SOUL",
            "BACK_TO_MAIN_MENU": "MAIN MENU",

            "PLAY": "PLAY",
            "OPTION": "OPTION",
            "EXIT": "EXIT",

            "SELECT_MODE": "SELECT MODE",
            "STORY_MODE": "STORY MODE",
            "DRAW_MODE": "DRAW MODE",

            "DRAW": "DRAW",
            "ERSE": "ERSE",
            "CLEAR_ALL": "CLEAR ALL",
            "SAVE": "SAVE",
            "SAVE_IN": "SAVE IN GAME FOLDER!",
            "HOW_PLAY": "HOW PLAY ->",
            "PRESS_ANY_KEY": "PRESS ANY KEY",

            "END_TXT_1": "At this moment, this strange story ends",
            "END_TXT_2": "Two souls together again, and ready to roam the empty, dead world",
            "END_TXT_3": "THANKS FOR PLAYING!"
        }
        with open("Language/" + "ENG.json", "w") as f:
            json.dump(data, f)

    LANG = os.listdir("Language")
    return LANG


LANGUAGE = check_lang()

txt_lang = None


def language(select):
    global txt_lang
    with open("Language/" + LANGUAGE[select]) as lang:
        txt_lang = None
        txt_lang = json.load(lang)
