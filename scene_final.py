import pygame
from config import sc, clock, WINDOW_SIZE
import scene_menu
import language_system


def scene():
    running = True
    FPS: int = 60

    txt = (language_system.txt_lang["END_TXT_1"],
           language_system.txt_lang["END_TXT_2"],
           language_system.txt_lang["END_TXT_3"])

    select = 0

    index_txt = 0

    frame_out = 0

    image_final = pygame.image.load("ASSETS/ui/final_screen.png").convert_alpha()
    image_final = pygame.transform.scale(image_final, (89 * 4, 76 * 4))

    frame_start = 0

    volume_audio = 0.2

    key_txt = pygame.font.Font("ASSETS/font/osifont.ttf", 25).render(language_system.txt_lang["PRESS_ANY_KEY"], True,
                                                                     (255, 255, 255))

    while running:
        txt_write = txt[select]
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.KEYDOWN:
                if select >= 2:
                    running = False
                    scene_menu.scene_m()
                if index_txt >= len(txt_write):
                    select += 1
                    index_txt = 0
        sc.fill((0, 0, 0))
        if frame_start >= 96:
            pygame.mixer.music.stop()
            if frame_out > 2:
                if index_txt < len(txt_write):
                    index_txt += 1
                frame_out = 0
            frame_out += 1
            if index_txt >= len(txt_write):
                sc.blit(key_txt, key_txt.get_rect(center=(WINDOW_SIZE[0] / 2, 655)))
            txt_draw = pygame.font.Font("ASSETS/font/osifont.ttf", 24).render(str(txt_write[:index_txt]), True,
                                                                              (255, 255, 255))
            sc.blit(txt_draw, txt_draw.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 + 230)))
            if select >= 1:
                sc.blit(image_final, image_final.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2)))
        else:
            if frame_start >= 45:
                volume_audio -= 0.01
                pygame.mixer.music.set_volume(volume_audio)
            frame_start += 1
        pygame.display.update()
        clock.tick(FPS)
