import pygame

import scene_game
import scene_menu
import scene_draw_mode
from config import sc, clock, WINDOW_SIZE
import language_system


def scene_how_play():
    running = True
    FPS: int = 60

    frame = 0

    select = 0

    mouse_image = pygame.image.load("ASSETS/ui/mouse.png").convert_alpha()
    mouse_image = pygame.transform.scale(mouse_image, (10 * 6, 18 * 6))

    xPosCircle = WINDOW_SIZE[0] / 2 - 250
    xPosMouse = WINDOW_SIZE[0] / 2 + 250

    yPosCircle = WINDOW_SIZE[1] / 2
    yPosMouse = WINDOW_SIZE[1] / 2

    how_play_txt = pygame.font.Font("ASSETS/font/osifont.ttf", 30).render(language_system.txt_lang["HOW_PLAY"], True,
                                                                          (255, 255, 255))
    key_txt = pygame.font.Font("ASSETS/font/osifont.ttf", 25).render(language_system.txt_lang["PRESS_ANY_KEY"], True,
                                                                     (255, 255, 255))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_RETURN or e.key == pygame.K_e:
                    select += 1
                    frame = 0

        sc.fill((0, 0, 0))

        if select == 0:
            if frame < 30:
                xPosMouse += 5
                xPosCircle += 5
            if frame > 30:
                xPosMouse -= 5
                xPosCircle -= 5
            if frame >= 60:
                xPosCircle = WINDOW_SIZE[0] / 2 - 250
                xPosMouse = WINDOW_SIZE[0] / 2 + 250
                frame = 0
            frame += 1
        if select == 1:
            xPosCircle = WINDOW_SIZE[0] / 2 - 250
            xPosMouse = WINDOW_SIZE[0] / 2 + 250
            if frame < 30:
                yPosMouse -= 5
                yPosCircle -= 5
            if frame > 30:
                yPosMouse += 5
                yPosCircle += 5
            if frame >= 60:
                yPosCircle = WINDOW_SIZE[1] / 2
                yPosMouse = WINDOW_SIZE[1] / 2
                frame = 0
            frame += 1
        if select == 2:
            yPosCircle = WINDOW_SIZE[1] / 2
            pygame.draw.circle(sc, (155, 155, 155), (WINDOW_SIZE[0] / 2 + 100, WINDOW_SIZE[1] / 2), 11 * 2)
            if frame < 60:
                xPosCircle += 5
            if frame >= 60:
                xPosCircle = WINDOW_SIZE[0] / 2 - 200
                frame = 0
            frame += 1
        if select >= 3:
            running = False
            scene_game.scene_g()

        if select < 2:
            sc.blit(mouse_image, mouse_image.get_rect(center=(xPosMouse, yPosMouse)))
        pygame.draw.circle(sc, (255, 255, 255), (xPosCircle, yPosCircle), 12 * 2)
        sc.blit(how_play_txt, how_play_txt.get_rect(center=(WINDOW_SIZE[0] / 2, 40)))
        sc.blit(key_txt, key_txt.get_rect(center=(WINDOW_SIZE[0] / 2, 650)))
        pygame.display.update()
        clock.tick(FPS)


def scene_s_m():
    running: bool = True
    FPS: int = 60

    story_mode_txt = pygame.font.Font("ASSETS/font/osifont.ttf", 35).render(language_system.txt_lang["STORY_MODE"],
                                                                            True, (255, 255, 255))
    draw_mode_txt = pygame.font.Font("ASSETS/font/osifont.ttf", 35).render(language_system.txt_lang["DRAW_MODE"], True,
                                                                           (255, 255, 255))

    select: int = 0

    selectorImage = pygame.image.load("ASSETS/ui/selector.png").convert_alpha()
    selectorImage = pygame.transform.scale(selectorImage, (43 * 5, 19 * 3))

    select_mode_txt = pygame.font.Font("ASSETS/font/osifont.ttf", 45).render(language_system.txt_lang["SELECT_MODE"],
                                                                             True, (205, 205, 205))

    yPosSelector: int = WINDOW_SIZE[1] / 2 - 50

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_RETURN:
                    if select == 0:
                        running = False
                        scene_how_play()
                    if select == 1:
                        running = False
                        scene_draw_mode.scene()
                if e.key == pygame.K_ESCAPE:
                    running = False
                    scene_menu.scene_m()
                if e.key == pygame.K_DOWN or e.key == pygame.K_s:
                    select += 1
                if e.key == pygame.K_UP or e.key == pygame.K_w:
                    select -= 1
                select %= 2

        sc.fill((0, 0, 0))
        if select == 0:
            yPosSelector: int = WINDOW_SIZE[1] / 2 - 50
        if select == 1:
            yPosSelector: int = WINDOW_SIZE[1] / 2 + 50

        sc.blit(story_mode_txt, story_mode_txt.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 - 50)))
        sc.blit(draw_mode_txt, draw_mode_txt.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 + 50)))

        sc.blit(select_mode_txt, select_mode_txt.get_rect(center=(WINDOW_SIZE[0] / 2, 75)))

        sc.blit(selectorImage, selectorImage.get_rect(center=(WINDOW_SIZE[0] / 2, yPosSelector - 5)))

        pygame.display.update()
        clock.tick(FPS)
