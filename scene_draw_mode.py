import pygame
import time

import language_system
from config import sc, clock, WINDOW_SIZE
from light import light_create
from particle import Particle
import scene_menu

particle_image = (pygame.image.load("ASSETS/sprite/particle/1_1.png").convert_alpha(),
                  pygame.image.load("ASSETS/sprite/particle/2_2.png").convert_alpha(),
                  pygame.image.load("ASSETS/sprite/particle/1.png").convert_alpha(),
                  pygame.image.load("ASSETS/sprite/particle/2.png").convert_alpha(),
                  pygame.image.load("ASSETS/sprite/particle/3.png").convert_alpha(),
                  pygame.image.load("ASSETS/sprite/particle/4.png").convert_alpha())

particle_group = pygame.sprite.Group()
bullet_group = pygame.sprite.Group()

fillGroup = pygame.sprite.Group()

isSaveDraw = False


def save_draw(surf):
    global isSaveDraw
    pygame.image.save(surf, str(int(time.time())) + ".png")
    isSaveDraw = False


class fillScreen(pygame.sprite.Sprite):
    def __init__(self, color, speed_fill=-5, alpha=250, alpha_max=0):
        super().__init__()
        self.image = pygame.Surface((WINDOW_SIZE[0], WINDOW_SIZE[1]))
        self.image.fill(color)
        self.rect = self.image.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2))

        self.add(fillGroup)

        self.alpha = alpha
        self.speed_fill = speed_fill
        self.alpha_max = alpha_max

    def update(self):
        if self.alpha <= self.alpha_max:
            self.kill()
        self.alpha += self.speed_fill
        self.image.set_alpha(self.alpha)


def scene():
    global isSaveDraw
    for particle in particle_group:
        particle.kill()

    running: bool = True
    FPS: int = 60

    circleRect = pygame.Rect(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2, 12, 12)

    colorFillScreen = (0, 0, 0)
    colorFillCircle = (255, 255, 255)

    pygame.mixer.music.set_volume(0.1)

    isPause: bool = False

    txtResume = pygame.font.Font("ASSETS/font/osifont.ttf", 30).render(language_system.txt_lang["RESUME"], True, (255, 255, 255))
    txtMainMenu = pygame.font.Font("ASSETS/font/osifont.ttf", 30).render(language_system.txt_lang["BACK_TO_MAIN_MENU"], True,
                                                                         (255, 255, 255))

    select: int = 0

    draw_surf = pygame.Surface((WINDOW_SIZE[0] - 100, WINDOW_SIZE[1] - 100))

    mouse_img = [pygame.image.load("ASSETS/ui/key/mouse.png").convert_alpha(),
                 pygame.image.load("ASSETS/ui/key/mouse.png").convert_alpha()]

    mouse_img[0] = pygame.transform.scale(mouse_img[0], (11 * 3, 14 * 3))
    mouse_img[1] = pygame.transform.scale(mouse_img[1], (11 * 3, 14 * 3))

    mouse_img[1] = pygame.transform.flip(mouse_img[1], True, False)

    txt_draw = pygame.font.Font("ASSETS/font/osifont.ttf", 15).render(language_system.txt_lang["DRAW"], True, (255, 255, 255))
    txt_erse = pygame.font.Font("ASSETS/font/osifont.ttf", 15).render(language_system.txt_lang["ERSE"], True, (255, 255, 255))

    key_g_image = pygame.image.load("ASSETS/ui/key/g.png").convert_alpha()

    key_g_image = pygame.transform.scale(key_g_image, (16 * 2, 16 * 2))

    key_f6_image = pygame.image.load("ASSETS/ui/key/f6.png").convert_alpha()

    key_f6_image = pygame.transform.scale(key_f6_image, (16 * 2, 16 * 2))

    txt_clear_all = pygame.font.Font("ASSETS/font/osifont.ttf", 15).render(language_system.txt_lang["CLEAR_ALL"], True, (255, 255, 255))

    txt_save = pygame.font.Font("ASSETS/font/osifont.ttf", 15).render(language_system.txt_lang["SAVE"], True, (255, 255, 255))

    txt_save_in = pygame.font.Font("ASSETS/font/osifont.ttf", 20).render(language_system.txt_lang["SAVE_IN"], True, (255, 255, 255))

    pygame.mixer.music.stop()

    time_safe_show = 90

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    isSaveDraw = True
                    time_safe_show = 0
                if e.key == pygame.K_ESCAPE:
                    if not isPause:
                        isPause = True
                    else:
                        isPause = False
                if isPause:
                    if e.key == pygame.K_RETURN or e.key == pygame.K_e:
                        if select == 0:
                            isPause = False
                        if select == 1:
                            running = False
                            scene_menu.scene_m()
                    if e.key == pygame.K_DOWN or e.key == pygame.K_s:
                        select += 1
                    if e.key == pygame.K_UP or e.key == pygame.K_w:
                        select -= 1
                    select %= 2

        sc.fill(colorFillScreen)
        if not isPause:
            sc.blit(draw_surf, draw_surf.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2)))
            draw_surf.fill((0, 0, 0))

            particle_group.draw(draw_surf)
            particle_group.update()

            if pygame.mouse.get_pressed()[0]:
                Particle(circleRect.centerx, circleRect.centery, 0, 0,
                         particle_image[5],
                         (6, 6), particle_group, 0, False, draw_surf)
            for particle in particle_group:
                if pygame.key.get_pressed()[pygame.K_g]:
                    particle.kill()
                if circleRect.colliderect(particle.rect):
                    if pygame.mouse.get_pressed()[2]:
                        particle.kill()

            if not isSaveDraw:
                xMouse, yMouse = pygame.mouse.get_pos()
                circleRect.centerx = xMouse
                circleRect.centery = yMouse
                pygame.draw.circle(draw_surf, colorFillCircle, circleRect.center, 12)

                draw_surf.blit(light_create(15, (55, 55, 55)), (circleRect.centerx - 15, circleRect.centery - 15),
                               special_flags=pygame.BLEND_RGB_ADD)
                draw_surf.blit(light_create(18, (52, 52, 52)), (circleRect.centerx - 18, circleRect.centery - 18),
                               special_flags=pygame.BLEND_RGB_ADD)
            else:
                save_draw(draw_surf)

            pygame.draw.rect(draw_surf, (255, 255, 255), (0, 0, WINDOW_SIZE[0] - 100, WINDOW_SIZE[1] - 100), 1)

            sc.blit(mouse_img[0], mouse_img[0].get_rect(center=(101, 675)))

            sc.blit(txt_draw, txt_draw.get_rect(center=(150 + len(language_system.txt_lang["DRAW"]) - 1, 675)))

            sc.blit(mouse_img[1], mouse_img[1].get_rect(center=(220, 675)))

            sc.blit(txt_erse, txt_erse.get_rect(center=(260 + len(language_system.txt_lang["ERSE"]), 675)))

            sc.blit(key_g_image, key_g_image.get_rect(center=(WINDOW_SIZE[0] / 2, 675)))
            sc.blit(txt_clear_all,
                    txt_clear_all.get_rect(center=(WINDOW_SIZE[0] / 2 + 55 + len(language_system.txt_lang["CLEAR_ALL"]), 675)))

            sc.blit(key_f6_image, key_f6_image.get_rect(center=(WINDOW_SIZE[0] / 2 + 210, 675)))
            sc.blit(txt_save,
                    txt_save.get_rect(center=(WINDOW_SIZE[0] / 2 + 200 + 55 + len(language_system.txt_lang["SAVE"]), 675)))

            fillGroup.draw(sc)
            fillGroup.update()

            if time_safe_show <= 58:
                time_safe_show += 1
                sc.blit(txt_save_in,
                        txt_save_in.get_rect(center=(WINDOW_SIZE[0] / 2, 25)))
        else:
            if select == 0:
                selectorImage = pygame.image.load("ASSETS/ui/selector.png").convert_alpha()
                selectorImage = pygame.transform.scale(selectorImage, (43 * 5, 19 * 3))
                yPosSelector: int = WINDOW_SIZE[1] / 2 - 55
            else:
                selectorImage = pygame.image.load("ASSETS/ui/selector.png").convert_alpha()
                selectorImage = pygame.transform.scale(selectorImage, (44 * 5, 19 * 3))
                yPosSelector: int = WINDOW_SIZE[1] / 2 + 45

            colorFillScreen = (0, 0, 0)
            sc.blit(txtResume, txtResume.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 - 50)))
            sc.blit(txtMainMenu, txtMainMenu.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 + 50)))

            sc.blit(selectorImage, selectorImage.get_rect(center=(WINDOW_SIZE[0] / 2, yPosSelector)))

        fpsTXT = pygame.font.Font("ASSETS/font/osifont.ttf", 15).render("FPS: " + str(int(clock.get_fps())), True,
                                                                        (150, 150, 150))
        sc.blit(fpsTXT, fpsTXT.get_rect(center=(30, 15)))

        pygame.display.update()
        clock.tick(FPS)
