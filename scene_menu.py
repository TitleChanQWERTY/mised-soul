import pygame

import language_system
import scene_select_mode

from config import sc, clock, WINDOW_SIZE


def scene_m():
    running: bool = True
    FPS: int = 60

    txtPlay = pygame.font.Font("ASSETS/font/osifont.ttf", 32).render(language_system.txt_lang["PLAY"], True, (255, 255, 255))
    txtExit = pygame.font.Font("ASSETS/font/osifont.ttf", 32).render(language_system.txt_lang["EXIT"], True, (255, 255, 255))

    txt_creator = pygame.font.Font("ASSETS/font/osifont.ttf", 20).render(language_system.txt_lang["CREATED_BY"], False, (255, 255, 255))
    txt_creator.set_alpha(150)

    frameAnim = 0
    timeFrameAnim = 0

    image_logo = [pygame.image.load("ASSETS/ui/logo/1.png").convert_alpha(),
                  pygame.image.load("ASSETS/ui/logo/2.png").convert_alpha(),
                  pygame.image.load("ASSETS/ui/logo/3.png").convert_alpha(),
                  pygame.image.load("ASSETS/ui/logo/4.png").convert_alpha(),
                  pygame.image.load("ASSETS/ui/logo/5.png").convert_alpha(),
                  pygame.image.load("ASSETS/ui/logo/6.png").convert_alpha(),
                  pygame.image.load("ASSETS/ui/logo/7.png").convert_alpha()]

    for i in range(len(image_logo)):
        image_logo[i] = pygame.transform.scale(image_logo[i], (72 * 4, 11 * 4))

    select = 0

    selectorImage = pygame.image.load("ASSETS/ui/selector.png").convert_alpha()
    selectorImage = pygame.transform.scale(selectorImage, (43 * 5, 19 * 3))

    yPosSelector: int = WINDOW_SIZE[1] / 2 - 55

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_RETURN:
                    running = False
                    if select == 0:
                        scene_select_mode.scene_s_m()
                if e.key == pygame.K_DOWN or e.key == pygame.K_s:
                    select += 1
                if e.key == pygame.K_UP or e.key == pygame.K_w:
                    select -= 1
                select %= 2

        sc.fill((0, 0, 0))
        if frameAnim < 6:
            if timeFrameAnim >= 4:
                timeFrameAnim = 0
                frameAnim += 1
            else:
                timeFrameAnim += 1
        else:
            sc.blit(txt_creator, txt_creator.get_rect(center=(WINDOW_SIZE[0] / 2, 650)))
        if frameAnim >= 5:
            if select == 0:
                yPosSelector: int = WINDOW_SIZE[1] / 2 - 50
            if select == 1:
                yPosSelector: int = WINDOW_SIZE[1] / 2 + 50

            sc.blit(txtPlay, txtPlay.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 - 50)))
            sc.blit(txtExit, txtExit.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 + 50)))

            sc.blit(selectorImage, selectorImage.get_rect(center=(WINDOW_SIZE[0]/2, yPosSelector-5)))

        sc.blit(image_logo[frameAnim], image_logo[frameAnim].get_rect(center=(WINDOW_SIZE[0]/2, 85)))
        pygame.display.update()
        clock.tick(FPS)
